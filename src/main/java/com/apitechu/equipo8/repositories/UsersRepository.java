package com.apitechu.equipo8.repositories;

import com.apitechu.equipo8.models.UsersModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends MongoRepository<UsersModel, String>{
}
