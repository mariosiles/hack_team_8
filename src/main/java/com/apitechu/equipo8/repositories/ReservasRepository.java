package com.apitechu.equipo8.repositories;

import com.apitechu.equipo8.models.ReservasModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservasRepository extends MongoRepository<ReservasModel, String>{
    //List<ReservasModel> findByUsers(String idUsers);
}



