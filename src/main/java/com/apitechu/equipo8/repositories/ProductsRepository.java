package com.apitechu.equipo8.repositories;

import com.apitechu.equipo8.models.ProductsModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductsRepository extends MongoRepository<ProductsModel, String> {
}
