package com.apitechu.equipo8.controllers;


import com.apitechu.equipo8.models.ProductsModel;
import com.apitechu.equipo8.services.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("equipo8/v1")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.PUT,RequestMethod.POST,RequestMethod.PATCH,RequestMethod.DELETE})
public class ProductsController {

    @Autowired
    ProductsService productsService;

    @GetMapping("/puestos")
    public List<ProductsModel> getProducts(){
        System.out.println("getProducts");

        return this.productsService.findAll();

    }

    @GetMapping("/puestos/{id}")
    public ResponseEntity<Object> getProductsById(@PathVariable String id){

        System.out.println("getProductById");
        System.out.println("La id del puesto a buscar es " + id);

        Optional<ProductsModel> result = this.productsService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Puesto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @GetMapping("/puestosordenados")
    public ResponseEntity<List<ProductsModel>> getProducts(@RequestParam (name = "$orderby",required = false) String orderBy){

        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productsService.getProducts(orderBy),
                HttpStatus.OK
        );
    }


    @PostMapping("/puestos")
    public ResponseEntity<ProductsModel> addProducts(@RequestBody ProductsModel products) {
        System.out.println("addProducts");
        System.out.println("La id del puesto que se va a crear es " + products.getId());
        System.out.println("El puesto que se va a crear es " + products.getPuesto());
        System.out.println("El edificio del puesto que se va a crear es " + products.getEdificio());
        System.out.println("La planta del puesto que se va a crear es " + products.getPlanta());


        return new ResponseEntity<>(this.productsService.add(products), HttpStatus.CREATED);
    }

    @PutMapping("/puestos")
    public ResponseEntity<Object> putProductsById(@RequestBody ProductsModel products){

        String id = products.getId();

        System.out.println("getProductById");
        System.out.println("La id del puesto a buscar es " + id);

        Optional<ProductsModel> result = this.productsService.findById(id);

        if (result.isPresent() == true){
            return new ResponseEntity<>(this.productsService.saveById(products), HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Puesto no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/puestos/{id}")
    public ResponseEntity<Object> deleteProductsById(@PathVariable String id){

        System.out.println("deleteProductsById");
        System.out.println("La id del puesto a borrar es " + id);

        Optional<ProductsModel> result = this.productsService.findById(id);

        if (result.isPresent() == true){
            return new ResponseEntity<>(this.productsService.deleteById(id), HttpStatus.OK);
        }else{
            return new ResponseEntity<>("Puesto no encontrado", HttpStatus.NOT_FOUND);
        }
    }
}
