package com.apitechu.equipo8.controllers;

import com.apitechu.equipo8.models.UsersModel;
import com.apitechu.equipo8.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("equipo8/v1")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.PUT,RequestMethod.POST,RequestMethod.PATCH,RequestMethod.DELETE})
public class UsersController {

    @Autowired
    UsersService usersService;

    @GetMapping("/users")
    public ResponseEntity<List<UsersModel>> getUsers(@RequestParam (name = "$orderby",required = false) String orderBy){

        System.out.println("getUsers");

        return new ResponseEntity<>(
                this.usersService.getUsers(orderBy),
                HttpStatus.OK
        );
    }


    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("getUserById");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UsersModel> result = this.usersService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UsersModel> updateUsers(@RequestBody UsersModel users, @PathVariable String id){
        System.out.println("updateUser");
        System.out.println("La id del usuario a actualizar es " + id);
        System.out.println("El nombre del usuario  a actualizar es " + users.getName());
        System.out.println("El departamento del usuario a actualizar es " + users.getDepartamento());
        System.out.println("El cargo del usuario a actualizar es " + users.getCargo());

        Optional<UsersModel> usersToUpdate = this.usersService.findById(id);

        if (usersToUpdate.isPresent() == true){
            System.out.println("Usuario para actualizar encontrado, actualizado");
            this.usersService.update(users);
        }

        return new ResponseEntity<>(users,
                usersToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/users")
    public ResponseEntity<UsersModel> addUsers(@RequestBody UsersModel users) {
        System.out.println("addUser");
        System.out.println("La id del usuario que se va a crear es " + users.getId());
        System.out.println("El nombre del usuario que se va a crear es " + users.getName());
        System.out.println("El departamento del usuario que se va a crear es " + users.getDepartamento());
        System.out.println("El cargo del usuario que se va a crear es " + users.getCargo());

        return new ResponseEntity<>(this.usersService.add(users), HttpStatus.CREATED);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUsers(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es " + id);

        boolean deletedUser = this.usersService.delete(id);

        return new ResponseEntity<>(
                deletedUser ? "Usuario borrado" : "Usuario no borrado",
                deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
