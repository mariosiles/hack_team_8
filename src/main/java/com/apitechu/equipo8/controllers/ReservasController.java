package com.apitechu.equipo8.controllers;


import com.apitechu.equipo8.models.ReservasModel;
import com.apitechu.equipo8.services.ReservasService;
import com.apitechu.equipo8.services.ReservasServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("equipo8/v1")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET,RequestMethod.PUT,RequestMethod.POST,RequestMethod.PATCH,RequestMethod.DELETE})
public class ReservasController {

    @Autowired
    ReservasService reservasService;

    @GetMapping("/reservas")
    public ResponseEntity<List<ReservasModel>> getReservas(@RequestParam (name = "$orderby",required = false) String orderBy){

        System.out.println("getReservas");

        return new ResponseEntity<>(
                this.reservasService.getReservas(orderBy),
                HttpStatus.OK
        );
    }

    @GetMapping("/reservas/{id}")
    public ResponseEntity<Object> getReservasById(@PathVariable String id){
        System.out.println("getReservasById");
        System.out.println("La id de la reserva a buscar es " + id);

        Optional<ReservasModel> result = this.reservasService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Reserva no encontrada",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //@GetMapping("/reservas/{idUsers}")
    //public ResponseEntity<List<ReservasModel>> getReservasByUser(@PathVariable String idUsers){
    //    System.out.println("getReservasByUser");
    //    System.out.println("El usuario a buscar es " + idUsers);

    //    Optional<ReservasModel> result = this.reservasService.findByUser(idUsers);

    //    return new ResponseEntity<>(
    //            result.isPresent() ? result.get() : "Reserva no encontrada",
    //            result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
    //    );
    //}


    @PutMapping("/reservas/{id}")
    public ResponseEntity<ReservasModel> updateReserva(@RequestBody ReservasModel reservas, @PathVariable String id){
        System.out.println("updateReserva");
        System.out.println("La id de la reserva a actualizar es " + id);
        System.out.println("El id del usuario de la reserva a actualizar es " + reservas.getIdUsers());
        System.out.println("El id del puesto de la reserva a actualizar es " + reservas.getIdPuesto());
        System.out.println("la fecha de alta de la reserva es " + reservas.getDateAlta());

        Optional<ReservasModel> reservasToUpdate = this.reservasService.findById(id);

        if (reservasToUpdate.isPresent() == true){
            System.out.println("Reserva para actualizar encontrada, actualizada");
            this.reservasService.update(reservas);
        }

        return new ResponseEntity<>(reservas,
                reservasToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @PostMapping("/reservas")
    public ResponseEntity<ReservasModel> addReservas(@RequestBody ReservasModel reservas) {
        System.out.println("addReserva");
        System.out.println("La id de la reserva que se va a crear es " + reservas.getId());
        System.out.println("El id del usuario de la reserva que se va a crear es " + reservas.getIdUsers());
        System.out.println("El id del puesto de la reserva que se crear es " + reservas.getIdPuesto());
        System.out.println("La fecha de alta de la reserva que se va a crear es " + reservas.getDateAlta());

        return new ResponseEntity<>(this.reservasService.add(reservas), HttpStatus.CREATED);
    }

    @DeleteMapping("/reservas/{id}")
    public ResponseEntity<String> deleteReservas(@PathVariable String id){
        System.out.println("deleteReservas");
        System.out.println("La id de la reserva a borrar es " + id);

        boolean deletedReservas = this.reservasService.delete(id);

        return new ResponseEntity<>(
                deletedReservas ? "Reserva borrada" : "Reserva no borrada",
                deletedReservas ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @GetMapping("/reservas/byuser/{id}")
    public ResponseEntity<ReservasServiceResponse> findAllbyUserID(@PathVariable String id){
        System.out.println("findAllbyUserID");

        ReservasServiceResponse result = this.reservasService.findAllbyUserID(id);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }


}
