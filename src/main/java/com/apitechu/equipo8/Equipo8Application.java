package com.apitechu.equipo8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Equipo8Application {

	public static void main(String[] args) {

		SpringApplication.run(Equipo8Application.class, args);
	}

}
