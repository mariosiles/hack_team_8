package com.apitechu.equipo8.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "reservas")
public class ReservasModel {

    @Id
    private String id;
    private String idUsers;
    private String idPuesto;
    private Date dateAlta;
    private Date dateBaja;

    public ReservasModel() {
    }

    public ReservasModel(String id, String idUsers, String idPuesto, Date dateAlta, Date dateBaja) {
        this.id = id;
        this.idUsers = idUsers;
        this.idPuesto = idPuesto;
        this.dateAlta = dateAlta;
        this.dateBaja = dateBaja;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(String idUsers) {
        this.idUsers = idUsers;
    }

    public String getIdPuesto() {
        return idPuesto;
    }

    public void setIdPuesto(String idPuesto) {
        this.idPuesto = idPuesto;
    }

    public Date getDateAlta() {
        return dateAlta;
    }

    public void setDateAlta(Date dateAlta) {
        this.dateAlta = dateAlta;
    }

    public Date getDateBaja() {
        return dateBaja;
    }

    public void setDateBaja(Date dateBaja) {
        this.dateBaja = dateBaja;
    }
}
