package com.apitechu.equipo8.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "products") //Esta es la definición del nombre de la collección en Mongo
public class ProductsModel {

    @Id
    private String id;
    private String puesto;
    private String edificio;
    private String planta;

    public ProductsModel() {

    }

    public ProductsModel(String id, String puesto, String edificio, String planta) {
        this.id = id;
        this.puesto = puesto;
        this.edificio = edificio;
        this.planta = planta;
    }

    public String getId() {
        return id;
    }

    public String getPuesto() {
        return puesto;
    }

    public String getEdificio() {
        return edificio;
    }

    public String getPlanta() {
        return planta;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public void setPlanta(String planta) {
        this.planta = planta;
    }
}
