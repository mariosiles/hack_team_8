package com.apitechu.equipo8.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users") //Esta es la definición del nombre de la collección en Mongo
public class UsersModel {

    @Id
    private String id;
    private String name;
    private String departamento;
    private String cargo;
    private String imagen;

    public UsersModel() {

    }

    public UsersModel(String id, String name, String departamento, String cargo) {
        this.id = id;
        this.name = name;
        this.departamento = departamento;
        this.cargo = cargo;
        this.imagen = imagen;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDepartamento() {
        return departamento;
    }

    public String getCargo() {
        return cargo;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}