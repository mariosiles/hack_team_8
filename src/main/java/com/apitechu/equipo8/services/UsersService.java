package com.apitechu.equipo8.services;

import com.apitechu.equipo8.repositories.UsersRepository;
import com.apitechu.equipo8.models.UsersModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsersService {

    @Autowired
    UsersRepository usersRepository;

    public List<UsersModel> getUsers(String orderBy) {
        System.out.println("getUsers");
        List<UsersModel> result;

        if (orderBy != null) {
            System.out.println("Se ha pedido ordenación");

            result = this.usersRepository.findAll(Sort.by("id"));
        } else {
            result = this.usersRepository.findAll();
        }

        return result;
    }

    public Optional<UsersModel> findById(String id){
        System.out.println("findById");
        System.out.println("Obteniendo el usuario con la id " + id);

        return this.usersRepository.findById(id);
    }

    public UsersModel add(UsersModel user){
        System.out.println("add");

        return this.usersRepository.save(user);
    }

    public UsersModel update(UsersModel user){
        System.out.println("update");
        System.out.println("Actualizando el usuario con la id " + user.getId());

        return this.usersRepository.save(user);
    }

    public boolean delete(String id){
        System.out.println("delete");
        boolean result = false;

        if (this.usersRepository.findById(id).isPresent() == true){
            System.out.println("Usuario encontrado, borrado");
            this.usersRepository.deleteById(id);
            result = true;
        };

        return result;
    }
}
