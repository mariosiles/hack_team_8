package com.apitechu.equipo8.services;


import com.apitechu.equipo8.models.ProductsModel;
import com.apitechu.equipo8.repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductsService {

    @Autowired
    ProductsRepository productsRepository;

    public List<ProductsModel> findAll() {
        return  this.productsRepository.findAll();
    }

    public Optional<ProductsModel> findById(String id){ //el optinal es porque puede ser que exista o no el valor en la bbdd, no es obligaria la existencia
        System.out.println("findById");
        System.out.println("Obteniendo el producto con la id " + id);

        return this.productsRepository.findById(id);
    }

    public ProductsModel add(ProductsModel product){
        System.out.println("add");

        return this.productsRepository.save(product); //save inserta y actualiza. Si no queremos actualización, utilizaremos insert.
    }

    public ProductsModel saveById(ProductsModel products){
        System.out.println("saveById");
        System.out.println("Actualizando el puesto con la id " + products.getId());

        return this.productsRepository.save(products);
    }

    public Optional<ProductsModel> deleteById(String id){
        System.out.println("deleteById");
        System.out.println("Eliminando el puesto con la id " + id);

        Optional<ProductsModel> result = this.productsRepository.findById(id);

        this.productsRepository.deleteById(id);

        return result;
    }

    public List<ProductsModel> getProducts(String orderBy) {
        System.out.println("getProducts");
        List<ProductsModel> result;

        if (orderBy != null) {
            System.out.println("Se ha pedido ordenación");

            result = this.productsRepository.findAll(Sort.by("puesto"));
        } else {
            result = this.productsRepository.findAll();
        }

        return result;
    }

}
