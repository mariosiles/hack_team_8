package com.apitechu.equipo8.services;

import org.springframework.http.HttpStatus;

import com.apitechu.equipo8.models.ReservasModel;

public class ReservasServiceResponse {

    private String msg;
    private ReservasModel reservas;
    private HttpStatus httpStatus;

    public ReservasServiceResponse() {

    }

    public String getMsg() {
        return msg;
    }

    public ReservasModel getReservas() {
        return reservas;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setReservas(ReservasModel reservas) {
        this.reservas = reservas;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}