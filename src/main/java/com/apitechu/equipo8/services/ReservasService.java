package com.apitechu.equipo8.services;

import com.apitechu.equipo8.models.ReservasModel;
import com.apitechu.equipo8.repositories.ReservasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class ReservasService {

    @Autowired
    ReservasRepository reservasRepository;

    public List<ReservasModel> getReservas(String orderBy) {
        System.out.println("getReservas");
        List<ReservasModel> result;

        if (orderBy != null) {
            System.out.println("Se ha pedido ordenación");

            result = this.reservasRepository.findAll(Sort.by(orderBy));
        } else {
            result = this.reservasRepository.findAll();
        }

        return result;
    }


    public Optional<ReservasModel> findById(String id) {
        System.out.println("findById");
        System.out.println("Obteniendo la reserva con la id " + id);

        return this.reservasRepository.findById(id);
    }

    public ReservasModel add(ReservasModel user) {
        System.out.println("add");

        return this.reservasRepository.save(user);
    }

    public ReservasModel update(ReservasModel user) {
        System.out.println("update");
        System.out.println("Actualizando la reserva con la id " + user.getId());

        return this.reservasRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.println("delete");
        boolean result = false;

        if (this.reservasRepository.findById(id).isPresent() == true) {
            System.out.println("Reserva encontrada, borrada");
            this.reservasRepository.deleteById(id);
            result = true;
        }
        ;

        return result;
    }


    public ReservasServiceResponse findAllbyUserID(String id) {
        ReservasServiceResponse result = new ReservasServiceResponse();
        List <ReservasModel> listUsers = new ArrayList<>();
        List <ReservasModel> allList = new ArrayList<>();

        allList = reservasRepository.findAll();
        System.out.println(allList.size());
        System.out.println("User buscado: " +id);
        for(int i=0; i<allList.size();i++){

            System.out.println(allList.get(i).getIdUsers());

            if (allList.get(i).getIdUsers().equals(id)){
                listUsers.add(allList.get(i));
            }

        }
        if(listUsers.isEmpty()){
            result.setMsg("No existen datos para ese cliente");
            result.setHttpStatus(HttpStatus.NO_CONTENT);
        }
        else{
            result.setReservas((ReservasModel) listUsers);
            result.setHttpStatus(HttpStatus.OK);
            result.setMsg("Listado Correcto");
        }

        return result;

    }

}